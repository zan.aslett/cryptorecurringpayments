async function main() {
    const [deployer] = await ethers.getSigners();

    console.log("Deploying contracts with the account:", deployer.address);

    console.log("Account balance:", (await deployer.getBalance()).toString());

    const ERC20Factory = await ethers.getContractFactory("SimpleErc20");
    const erc20Contract = await ERC20Factory.deploy(["0xD2eA9225005F7ee9E493D539521F5052368D57C9", "0x55E2cea12e847C1F2B0952462B8f9De351F41362"]);

    console.log("Token address:", erc20Contract.address);
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });

// Simple ERC20 deployed at:
// 0x67726ad7b3E1Ed6A0f68f57aC29315feF226FFF9
