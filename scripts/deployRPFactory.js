async function main() {
    const [deployer] = await ethers.getSigners();

    console.log("Deploying contracts with the account:", deployer.address);

    console.log("Account balance:", (await deployer.getBalance()).toString());

    const RPFactoryFactory = await ethers.getContractFactory("RPFactory");
    const rpFactory = await RPFactoryFactory.deploy();

    console.log("Token address:", rpFactory.address);
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });

// RPFactory deployed at:
// 0x5f49E61dC2f08462c72A67Fcc7293D4bD46d9231
// and
// 0xF2321B2dbe1cC8c48B01344C69549666d5Fd1c4b