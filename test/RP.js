const { expect } = require("chai");
const { ethers } = require("hardhat");
const { RP } = require("/artifacts/contracts/RP.sol");

describe("RP contract", function () {
    const DAY_SECONDS = 60 * 60 * 24;
    let customer1;
    let customer2;
    let customer3;
    let customer4;
    let merchant1;
    let merchant2;
    let redsky;

    let erc20Contract;
    let rpContract;

    before(async function () {
        [customer1, customer2, customer3, customer4, merchant1, merchant2, redsky] = await ethers.getSigners();
        erc20Contract = await (await ethers.getContractFactory("SimpleErc20")).deploy([customer1.address, customer2.address, customer3.address, customer4.address]);
    })

    describe("Simple ERC20 Gas Estimator", function() {
        it("should transfer some erc20 tokens", async function () {
            await erc20Contract.connect(customer3).approve(customer4.address, 9000);
            await erc20Contract.connect(customer4).transferFrom(customer3.address, customer2.address, 20);
            await erc20Contract.connect(customer4).transferFrom(customer3.address, customer2.address, 20);
            await erc20Contract.connect(customer4).transferFrom(customer3.address, customer2.address, 20);
        })
    })

    describe("Simple Subscription -- No Owner Fee", function () {
        before(async function () {
            let factory = await ethers.getContractFactory("RP");
            rpContract = await factory.connect(redsky).deploy(merchant1.address, [erc20Contract.address], [DAY_SECONDS, DAY_SECONDS * 2], 0);
        })

        it("should create a subscription", async function () {
            await erc20Contract.connect(customer1).approve(rpContract.address, 100000000);
            await rpContract.connect(customer1).createSubscription(10, 0, 0);
            let merchantBalance = await erc20Contract.balanceOf(merchant1.address);
            expect(merchantBalance).to.equal('10');
        })

        it("should charge a subscription", async function () {
            await ethers.provider.send("evm_increaseTime", [DAY_SECONDS]);
            await ethers.provider.send("evm_mine");
            await rpContract.connect(merchant1).chargeSubscription(0);
            let merchantBalance = await erc20Contract.balanceOf(merchant1.address);
            expect(merchantBalance).to.equal('20');
        })

        it("should fail to charge a subscription from non-merchant address", async function () {
            await expect(rpContract.connect(merchant2).chargeSubscription(0)).to.be.revertedWith("Only merchants can call this function");

            let merchantBalance = await erc20Contract.balanceOf(merchant1.address);
            expect(merchantBalance).to.equal('20');

            merchantBalance = await erc20Contract.balanceOf(merchant2.address);
            expect(merchantBalance).to.equal('0');
        })

        it("should fail to charge a subscription due to timelock", async function () {
            await expect(rpContract.connect(merchant1).chargeSubscription(0)).to.be.revertedWith("Time lock not yet released. Unable to charge this subscription.");

            let merchantBalance = await erc20Contract.balanceOf(merchant1.address);
            expect(merchantBalance).to.equal('20');

            merchantBalance = await erc20Contract.balanceOf(merchant2.address);
            expect(merchantBalance).to.equal('0');
        })

        it("should get the remaining time", async function () {
            const timeLock = await rpContract.connect(merchant1).getTimeRemaining(0);
            expect(DAY_SECONDS - timeLock).to.be.lt(15);
        })

        it("should update the merchant address", async function () {
            await rpContract.connect(merchant1).updateMerchantAddress(merchant2.address);
            let pendingUpdate = await rpContract.connect(merchant1).pendingMerchantAddressUpdate();
            let currentMerchant = await rpContract.connect(merchant1).merchant();
            expect(pendingUpdate).to.equal(merchant2.address);
            expect(currentMerchant).to.equal(merchant1.address);

            await rpContract.connect(merchant1).confirmMerchantAddressUpdate(merchant2.address);
            pendingUpdate = await rpContract.connect(merchant1).pendingMerchantAddressUpdate();
            currentMerchant = await rpContract.connect(merchant1).merchant();
            expect(pendingUpdate).to.not.equal(merchant2.address);
            expect(currentMerchant).to.equal(merchant2.address);
        })

        it("should force update the merchant address by the owner", async function () {
            await rpContract.connect(redsky).forceMerchantAddressUpdate(merchant1.address);
            let pendingUpdate = await rpContract.connect(merchant1).pendingMerchantAddressUpdate();
            let currentMerchant = await rpContract.connect(merchant1).merchant();
            expect(pendingUpdate).to.not.equal(merchant2.address);
            expect(currentMerchant).to.equal(merchant1.address);
        })

        it("should cancel a subscription by the customer", async function() {
            await rpContract.connect(customer1).cancelSubscription(0);
            await expect(rpContract.connect(merchant1).chargeSubscription(0)).to.be.revertedWith("Invalid subscriptionId. Perhaps it was canceled.");
        })
    })

    describe("Subscription With Owner Fee", function() {
        let subCost = 10000;
        let ownerFee = 150;

        let getMerchantRevenuePerCharge = () => {
            return (10000 - ownerFee) * subCost / 10000
        };
        let getRedskyRevenuePerCharge = () => {
            return (ownerFee) * subCost / 10000
        };

        before(async function () {
            let factory = await ethers.getContractFactory("RP");
            rpContract = await factory.connect(redsky).deploy(merchant2.address, [erc20Contract.address], [DAY_SECONDS, DAY_SECONDS * 2], ownerFee);
        })

        it("should create a subscription", async function () {
            await erc20Contract.connect(customer1).approve(rpContract.address, 100000000);
            await rpContract.connect(customer1).createSubscription(subCost, 0, 0);
            let merchantBalance = await erc20Contract.balanceOf(merchant2.address);
            let redskyBalance = await erc20Contract.balanceOf(redsky.address);
            expect(merchantBalance).to.equal(getMerchantRevenuePerCharge());
            expect(redskyBalance).to.equal(getRedskyRevenuePerCharge());
        })

        it("should charge a subscription", async function () {
            let merchantBalance = await erc20Contract.balanceOf(merchant2.address);
            let redskyBalance = await erc20Contract.balanceOf(redsky.address);
            expect(merchantBalance).to.equal(getMerchantRevenuePerCharge());
            expect(redskyBalance).to.equal(getRedskyRevenuePerCharge());

            await ethers.provider.send("evm_increaseTime", [DAY_SECONDS]);
            await ethers.provider.send("evm_mine");
            await rpContract.connect(merchant2).chargeSubscription(0);

            merchantBalance = await erc20Contract.balanceOf(merchant2.address);
            redskyBalance = await erc20Contract.balanceOf(redsky.address);
            expect(merchantBalance).to.equal(getMerchantRevenuePerCharge() * 2);
            expect(redskyBalance).to.equal(getRedskyRevenuePerCharge() * 2);
        })

        it("should fail to update the owner fee", async function () {
            await expect(rpContract.connect(redsky).setOwnerFee(10000)).to.be.revertedWith("Cannot set a fee higher than 5%.")
        })

        it("should update the owner fee", async function () {
            ownerFee = 200;
            await rpContract.connect(redsky).setOwnerFee(ownerFee);
            let realOwnerFee = await rpContract.connect(redsky).ownerFeeBp();
            expect(realOwnerFee).to.equal(ownerFee)
        })

        it("should charge a subscription with new fee", async function () {
            let startMerchantBalance = await erc20Contract.balanceOf(merchant2.address);
            let startRedskyBalance = await erc20Contract.balanceOf(redsky.address);

            await ethers.provider.send("evm_increaseTime", [DAY_SECONDS]);
            await ethers.provider.send("evm_mine");
            await rpContract.connect(merchant2).chargeSubscription(0);

            let merchantBalance = await erc20Contract.balanceOf(merchant2.address);
            let redskyBalance = await erc20Contract.balanceOf(redsky.address);
            expect(merchantBalance).to.equal(startMerchantBalance.add((10000 - ownerFee) * subCost / 10000));
            expect(redskyBalance).to.equal(startRedskyBalance.add((ownerFee) * subCost / 10000));
        })
    })

})