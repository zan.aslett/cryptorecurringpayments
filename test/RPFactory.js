const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("RPFactory contract", function () {
    let customer1;
    let customer2;
    let customer3;
    let customer4;
    let merchant1;
    let merchant2;
    let redsky;

    let erc20Contract;
    let rpFactoryContract;

    before(async function () {
        [customer1, customer2, customer3, customer4, merchant1, merchant2, redsky] = await ethers.getSigners();
        erc20Contract = await (await ethers.getContractFactory("SimpleErc20")).deploy([customer1.address, customer2.address, customer3.address, customer4.address]);
    })

    describe("Deploy simple merchant contract", function () {
        before(async function () {
            let factory = await ethers.getContractFactory("RPFactory");
            rpFactoryContract = await factory.deploy();
        })

        it("should create a new merchant", async function () {
            const transaction = await rpFactoryContract.connect(merchant1).deployRP([erc20Contract.address], [0, 60, 120], 150);
            const receipt = await transaction.wait();
            const event = receipt.events.find(event => event.event === 'RPContractDeployed');
            const [id, merchant, contractAddress] = event.args;
            expect(id).to.equal("0");
            expect(merchant).to.equal(merchant1.address);
        })
    })
})