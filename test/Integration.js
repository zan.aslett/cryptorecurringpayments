const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("RPFactory/RP Contracts Integration", function () {
    const DAY_SECONDS = 60 * 60 * 24;
    let customer1;
    let customer2;
    let customer3;
    let customer4;
    let merchant1;
    let merchant2;
    let redsky;

    let erc20Contract;
    let rpFactoryContract;

    before(async function () {
        [customer1, customer2, customer3, customer4, merchant1, merchant2, redsky] = await ethers.getSigners();
        erc20Contract = await (await ethers.getContractFactory("SimpleErc20")).deploy([customer1.address, customer2.address, customer3.address, customer4.address]);
        let factory = await ethers.getContractFactory("RPFactory");
        rpFactoryContract = await factory.connect(redsky).deploy();

    })

    describe("Simple Subscription -- No Owner Fee", function () {
        let rpFactory;
        let rpContract;

        before(async function () {
            const transaction = await rpFactoryContract.connect(merchant1).deployRP([erc20Contract.address], [DAY_SECONDS, DAY_SECONDS * 2], 150);
            const receipt = await transaction.wait();
            const event = receipt.events.find(event => event.event === 'RPContractDeployed');
            const [id, merchant, contractAddress] = event.args;

            rpFactory = await ethers.getContractFactory("RP");
            rpContract = await rpFactory.attach(contractAddress);
        })

        it("should create a subscription", async function () {
            await erc20Contract.connect(customer1).approve(rpContract.address, 100000000);
            await rpContract.connect(customer1).createSubscription(10, 0, 0);
            let merchantBalance = await erc20Contract.balanceOf(merchant1.address);
            expect(merchantBalance).to.equal('10');
        })

        it("should charge a subscription", async function () {
            await ethers.provider.send("evm_increaseTime", [DAY_SECONDS]);
            await ethers.provider.send("evm_mine");
            await rpContract.connect(merchant1).chargeSubscription(0);
            let merchantBalance = await erc20Contract.balanceOf(merchant1.address);
            expect(merchantBalance).to.equal('20');
        })

        it("should fail to charge a subscription", async function () {
            await expect(rpContract.connect(merchant2).chargeSubscription(0)).to.be.revertedWith("Only subscription participants can call this function");

            let merchantBalance = await erc20Contract.balanceOf(merchant1.address);
            expect(merchantBalance).to.equal('20');

            merchantBalance = await erc20Contract.balanceOf(merchant2.address);
            expect(merchantBalance).to.equal('0');
        })

        it("should cancel a subscription by the customer", async function() {
            await rpContract.connect(customer1).cancelSubscription(0);
            await expect(rpContract.connect(merchant1).chargeSubscription(0)).to.be.revertedWith("Invalid subscriptionId. Perhaps it was canceled.");
        })
    })

    describe("Advanced Factory Controls", async function () {
        let rpFactory;
        let rpContract;
        let id, merchant, contractAddress;

        before(async function () {
            const transaction = await rpFactoryContract.connect(merchant1).deployRP([erc20Contract.address], [DAY_SECONDS, DAY_SECONDS * 2], 150);
            const receipt = await transaction.wait();
            const event = receipt.events.find(event => event.event === 'RPContractDeployed');
            [id, merchant, contractAddress] = event.args;

            rpFactory = await ethers.getContractFactory("RP");
            rpContract = await rpFactory.attach(contractAddress);
        })

        afterEach(async function () {
            await ethers.provider.send("evm_increaseTime", [DAY_SECONDS]);
            await ethers.provider.send("evm_mine");
        })

        it("should create a subscription", async function () {
            await erc20Contract.connect(customer1).approve(rpContract.address, 100000000);

            let preMerchantBalance = await erc20Contract.balanceOf(merchant1.address);
            await rpContract.connect(customer1).createSubscription(10, 0, 0);
            let postMerchantBalance = await erc20Contract.balanceOf(merchant1.address);
            expect(postMerchantBalance).to.be.gt(preMerchantBalance);
        })

        it("should lock an RP contract", async function () {
            await rpFactoryContract.connect(redsky).setRPLock(id, true);
            let isLocked = await rpContract.isLocked();
            expect(isLocked);
        })

        it("should fail to charge in a locked contract", async function () {
            await expect(rpContract.connect(merchant1).chargeSubscription(0)).to.be.revertedWith("For security purposes, this contract has been locked.");
        })

        it("should fail to create a new subscription in a locked contract", async function () {
            await expect(rpContract.connect(customer2).createSubscription(10, 0, 0)).to.be.revertedWith("For security purposes, this contract has been locked.");
        })

        it("should unlock an RP contract, charge and create a new subscription", async function () {
            await rpFactoryContract.connect(redsky).setRPLock(id, false);
            let isLocked = await rpContract.isLocked();
            expect(!isLocked);

            let preMerchantBalance = await erc20Contract.balanceOf(merchant1.address);
            await rpContract.connect(merchant1).chargeSubscription(0);
            let postMerchantBalance = await erc20Contract.balanceOf(merchant1.address);
            expect(postMerchantBalance).to.be.gt(preMerchantBalance);


            await erc20Contract.connect(customer2).approve(rpContract.address, 100000000);
            preMerchantBalance = await erc20Contract.balanceOf(merchant1.address);
            await rpContract.connect(customer2).createSubscription(10, 0, 0);
            postMerchantBalance = await erc20Contract.balanceOf(merchant1.address);
            expect(postMerchantBalance).to.be.gt(preMerchantBalance);
        })

        it("should cancel a subscription from the factory", async function () {
            await rpFactoryContract.connect(redsky).proxyCancelSubscription(id, 0);
            await expect(rpContract.connect(merchant1).chargeSubscription(0)).to.be.revertedWith("Invalid subscriptionId. Perhaps it was canceled.");

            let preMerchantBalance = await erc20Contract.balanceOf(merchant1.address);
            await rpContract.connect(merchant1).chargeSubscription(1);
            let postMerchantBalance = await erc20Contract.balanceOf(merchant1.address);
            expect(postMerchantBalance).to.be.gt(preMerchantBalance);
        })

    })

    describe("Owner Fees to Factory", function() {
        let subCost = 10000;
        let ownerFee = 150;

        let getMerchantRevenuePerCharge = () => {
            return (10000 - ownerFee) * subCost / 10000
        };
        let getRedskyRevenuePerCharge = () => {
            return (ownerFee) * subCost / 10000
        };
        let rpFactory;
        let rpContract;
        let id, merchant, contractAddress;

        before(async function () {
            const transaction = await rpFactoryContract.connect(merchant1).deployRP([erc20Contract.address], [DAY_SECONDS, DAY_SECONDS * 2], 150);
            const receipt = await transaction.wait();
            const event = receipt.events.find(event => event.event === 'RPContractDeployed');
            [id, merchant, contractAddress] = event.args;

            rpFactory = await ethers.getContractFactory("RP");
            rpContract = await rpFactory.attach(contractAddress);
        })

        it("should change merchant address by factory", async function () {
            let oldMerchant = await rpContract.merchant();
            expect(oldMerchant).to.equal(merchant1.address);
            await rpFactoryContract.connect(redsky).dangerouslyForceSetMerchantAddress(id, merchant2.address);
            let newMerchant = await rpContract.merchant();
            expect(newMerchant).to.equal(merchant2.address);

            expect(oldMerchant).to.not.equal(newMerchant);
        })


        it("should create a subscription", async function () {
            await erc20Contract.connect(customer1).approve(rpContract.address, 100000000);
            await rpContract.connect(customer1).createSubscription(subCost, 0, 0);
            let merchantBalance = await erc20Contract.balanceOf(merchant2.address);
            let factoryBalance = await erc20Contract.balanceOf(rpFactoryContract.address);
            expect(merchantBalance).to.equal(getMerchantRevenuePerCharge());
            expect(factoryBalance).to.equal(getRedskyRevenuePerCharge());
        })

        it("should charge a subscription", async function () {
            let merchantBalance = await erc20Contract.balanceOf(merchant2.address);
            let factoryBalance = await erc20Contract.balanceOf(rpFactoryContract.address);
            expect(merchantBalance).to.equal(getMerchantRevenuePerCharge());
            expect(factoryBalance).to.equal(getRedskyRevenuePerCharge());

            await ethers.provider.send("evm_increaseTime", [DAY_SECONDS]);
            await ethers.provider.send("evm_mine");
            await rpContract.connect(merchant2).chargeSubscription(0);

            merchantBalance = await erc20Contract.balanceOf(merchant2.address);
            factoryBalance = await erc20Contract.balanceOf(rpFactoryContract.address);
            expect(merchantBalance).to.equal(getMerchantRevenuePerCharge() * 2);
            expect(factoryBalance).to.equal(getRedskyRevenuePerCharge() * 2);
        })

        it("should update the owner fee", async function () {
            ownerFee = 200;
            await rpFactoryContract.connect(redsky).setFactoryFee(id, ownerFee);
            let realOwnerFee = await rpContract.connect(redsky).ownerFeeBp();
            expect(realOwnerFee).to.equal(ownerFee)
        })

        it("should charge a subscription with new fee", async function () {
            let startMerchantBalance = await erc20Contract.balanceOf(merchant2.address);
            let startFactoryBalance = await erc20Contract.balanceOf(rpFactoryContract.address);

            await ethers.provider.send("evm_increaseTime", [DAY_SECONDS]);
            await ethers.provider.send("evm_mine");
            await rpContract.connect(merchant2).chargeSubscription(0);

            let merchantBalance = await erc20Contract.balanceOf(merchant2.address);
            let factoryBalance = await erc20Contract.balanceOf(rpFactoryContract.address);
            expect(merchantBalance).to.equal(startMerchantBalance.add((10000 - ownerFee) * subCost / 10000));
            expect(factoryBalance).to.equal(startFactoryBalance.add((ownerFee) * subCost / 10000));
        })

        it("should withdraw tokens from the factory", async function () {
            let preRsBalance = await erc20Contract.balanceOf(redsky.address);
            await rpFactoryContract.connect(redsky).withdrawFunds(erc20Contract.address);
            let postRsBalance = await erc20Contract.balanceOf(redsky.address);
            expect(preRsBalance).to.be.lt(postRsBalance);

        })
    })

})