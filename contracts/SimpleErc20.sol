// SPDX-License-Identifier: MIT
pragma solidity ^0.8.12;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract SimpleErc20 is IERC20 {
  string public name;
  string public symbol;
  uint8 public decimals; // 18 decimals is the strongly suggested default, avoid changing it

  uint256 public _totalSupply;

  mapping(address => uint) balances;
  mapping(address => mapping(address => uint)) allowed;

  /**
   * Constrctor function
   *
   * Initializes contract with initial supply tokens to the creator of the contract
   */
  constructor(address[] memory funded) {
    name = "Aslettokenz";
    symbol = "ATZ";
    decimals = 18;
    _totalSupply = 100000000000000000000000000;

    balances[msg.sender] = _totalSupply;
    for (uint i = 0; i < funded.length; i++) {
      balances[funded[i]] = _totalSupply / funded.length;
    }
    emit Transfer(address(0), msg.sender, _totalSupply);
  }

  function totalSupply() public view returns (uint) {
    return _totalSupply  - balances[address(0)];
  }

  function balanceOf(address tokenOwner) public view returns (uint balance) {
    return balances[tokenOwner];
  }

  function allowance(address tokenOwner, address spender) public view returns (uint remaining) {
    return allowed[tokenOwner][spender];
  }

  function approve(address spender, uint tokens) public returns (bool success) {
    allowed[msg.sender][spender] = tokens;
    emit Approval(msg.sender, spender, tokens);
    return true;
  }

  function transfer(address to, uint tokens) public returns (bool success) {
    balances[msg.sender] = balances[msg.sender] - tokens;
    balances[to] = balances[to] + tokens;
    emit Transfer(msg.sender, to, tokens);
    return true;
  }

  function transferFrom(address from, address to, uint tokens) public returns (bool success) {
    balances[from] = balances[from] - tokens;
    allowed[from][msg.sender] = allowed[from][msg.sender] - tokens;
    balances[to] = balances[to] + tokens;
    emit Transfer(from, to, tokens);
    return true;
  }
}
