// SPDX-License-Identifier: MIT
pragma solidity >=0.8.12;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

/// @title Source code for an RP contract (deployed by RPFactory)
/// @author Zan Aslett - RedSky Technologies
/// @notice This contract facilitates recurring payments and serves as a proxy between a merchant and customer
contract RP is Ownable {

  /// @dev Fits neatly into two 256-bit words for storage
  struct Subscription {
    uint256 cost;
    address customer; //160 bits
    uint80 chargeableAfterTime;
    uint8 cooldownIdx;
    uint8 tokenIdx;
  }

  Subscription[] public subscriptions;
  uint256[] public cooldowns;
  /// @notice This is the fee in basis points (bp). Subscriptio0.01% = 1bp
  uint256 public ownerFeeBp;
  address[] public tokens;
  address public merchant;
  address public pendingMerchantAddressUpdate;
  bool public isLocked;

  event SubscriptionCreated (
    uint256 indexed subscriptionId,
    uint256 cost,
    address indexed customer,
    uint80 chargeableAfterTime,
    uint8 cooldownIdx,
    uint8 tokenIdx
  );

  event SubscriptionCancelled(
    uint256 indexed subscriptionId
  );

  event SubscriptionPaid(
    uint256 indexed subscriptionId,
    uint80 chargeableAfterTime
  );

  event MerchantAddressUpdate(
    address indexed newMerchant
  );

  event PendingMerchantAddressUpdate(
    address indexed pendingNewMerchant
  );

  event OwnerFeeChanged(
    uint256 amount
  );

  modifier onlyMerchant() {
    require(msg.sender == merchant, "Only merchants can call this function");
    _;
  }

  /// @notice Callable by the customer, merchant, or contract owner
  modifier onlyParticipant(uint256 subscriptionId) {
    require(msg.sender == merchant || msg.sender == subscriptions[subscriptionId].customer || msg.sender == owner(), "Only subscription participants can call this function");
    _;
  }

  /// @notice Ensures the contract has not been locked by its owner for security reasons
  modifier notLocked() {
    require(!isLocked, 'For security purposes, this contract has been locked.');
    _;
  }

  /// @param _merchant The address of the merchant for this contract
  /// @param _tokens A list of erc20 token addresses that are accepted by this contract
  /// @param _cooldowns A list of acceptable cooldowns for a subscription given in seconds. Minimum cooldown is 1 day
  /// @param _ownerFeeBp The owner's fee represented in basis points. 0.01% = 1bp
  constructor(address _merchant, address[] memory _tokens, uint256[] memory _cooldowns, uint256 _ownerFeeBp) {
    require(_ownerFeeBp <= 500, "Cannot set a fee higher than 5%.");
    merchant = _merchant;
    tokens = _tokens;
    cooldowns = _cooldowns;
    ownerFeeBp = _ownerFeeBp;
  }

  /// @notice Callable by the contract owner
  /// @dev (Un)Locks the contract down completely
  /// @param _isLocked New value of the contract's locked
  function setIsLocked(bool _isLocked) external onlyOwner {
    isLocked = _isLocked;
  }

  /// @notice To be called by the end user
  /// @notice Before calling this function, please approve `address(this)` to withdraw erc20 tokens via `_tokens[_tokenIdx]`
  /// @dev Re-entrant resistant due to checks-effects-interactions pattern
  /// @param _subscriptionCost The cost of the subscription
  /// @param _cooldownIdx The index into cooldowns to be used for this subscription
  /// @param _tokenIdx The index into tokens to be used for this subscription
  function createSubscription(
    uint256 _subscriptionCost,
    uint8 _cooldownIdx,
    uint8 _tokenIdx
  )
  external virtual notLocked
  {
    // Checks

    // Effects
    Subscription memory subscription = Subscription(
      _subscriptionCost,
      msg.sender,
      uint80(block.timestamp + getCooldown(_cooldownIdx)),
      _cooldownIdx,
      _tokenIdx
    );
    subscriptions.push(subscription);

    // Interactions
    IERC20 tokenInterface;
    tokenInterface = IERC20(tokens[_tokenIdx]);
    uint256 factoryCut = getOwnerCut(_subscriptionCost, ownerFeeBp);
    require(tokenInterface.transferFrom(msg.sender, merchant, _subscriptionCost - factoryCut), "Initial subscription payment failed.");
    require(tokenInterface.transferFrom(msg.sender, owner(), factoryCut), "Initial subscription payment failed.");

    // Notifications
    emit SubscriptionCreated(subscriptions.length - 1, subscription.cost, subscription.customer, subscription.chargeableAfterTime,  subscription.cooldownIdx, subscription.tokenIdx);
    emit SubscriptionPaid(subscriptions.length - 1, subscription.chargeableAfterTime);

  }

  /// @notice A subscription may be cancelled at any time
  /// @param _subscriptionId The id of the subscription to be cancelled
  function cancelSubscription(uint256 _subscriptionId) external virtual onlyParticipant(_subscriptionId) {
    delete subscriptions[_subscriptionId];
    emit SubscriptionCancelled(_subscriptionId);
  }

  /// @notice To be called by the merchant
  /// @notice Charges a subscription object. Reverts if insufficient balance or erc20 allowance, or if time locked
  /// @dev Re-entrant resistant due to checks-effects-interactions pattern
  function chargeSubscription(uint256 _subscriptionId) external virtual onlyParticipant(_subscriptionId) notLocked {
    // Checks
    require(subscriptions[_subscriptionId].customer != address(0), "Invalid subscriptionId. Perhaps it was canceled.");
    require(subscriptions[_subscriptionId].chargeableAfterTime < block.timestamp, "Time lock not yet released. Unable to charge this subscription.");

    // Effects
    subscriptions[_subscriptionId].chargeableAfterTime = uint80(block.timestamp + getCooldown(subscriptions[_subscriptionId].cooldownIdx));

    // Interactions
    uint256 cost = subscriptions[_subscriptionId].cost;
    uint256 ownerCut = getOwnerCut(cost, ownerFeeBp);
    IERC20 tokenInterface = IERC20(tokens[subscriptions[_subscriptionId].tokenIdx]);
    require(tokenInterface.transferFrom(subscriptions[_subscriptionId].customer, merchant, cost - ownerCut), "Subscription payment failed.");
    require(tokenInterface.transferFrom(subscriptions[_subscriptionId].customer, owner(), ownerCut), "Subscription payment failed.");

    //Notifications
    emit SubscriptionPaid(_subscriptionId, subscriptions[_subscriptionId].chargeableAfterTime);
  }

  /// @notice To be called by contract owner. Fee may not exceed 5%
  /// @param _bp Basis points of owner fee. 0.01% = 1bp
  function setOwnerFee(uint256 _bp) external onlyOwner {
    require(_bp <= 500, "Cannot set a fee higher than 5%.");
    ownerFeeBp = _bp;
    emit OwnerFeeChanged(ownerFeeBp);
  }

  /// @notice Warning: Updating the merchant address will cause the current merchant to lose all privileges
  /// @notice Calling this function only sets a pending update. Call confirmMerchantAddressUpdate to confirm changes
  /// @dev Can call this function with address(0) to revert pending changes
  function updateMerchantAddress(address _merchant) external onlyMerchant {
    pendingMerchantAddressUpdate = _merchant;
    emit PendingMerchantAddressUpdate(pendingMerchantAddressUpdate);
  }

  /// @notice WARNING: This function will permanently update the merchant address. Make sure you have control over the new address
  /// @notice updateMerchantAddress must be called to create a pending update before calling this function
  /// @notice Call updateMerchantAddress with address(0) to revert pending changes
  function confirmMerchantAddressUpdate(address _merchant) external onlyMerchant {
    require(pendingMerchantAddressUpdate == _merchant, "Pending address update does not match confirmation address.");
    setMerchantAddress(_merchant);
  }

  /// @notice This is to be used in customer support roles only in the case of losing/breaching account access of merchant
  function forceMerchantAddressUpdate(address _merchant) external onlyOwner {
    setMerchantAddress(_merchant);
  }

  /// @notice Returns the time remaining on the timelock in seconds
  /// @dev There are minor fluctuations in block timestamp. These are insignificant as long as cooldown >> ~15 seconds
  function getTimeRemaining(uint256 _subscriptionId) external view virtual returns(uint256) {
    require(subscriptions[_subscriptionId].customer != address(0), "Invalid subscriptionId. Perhaps it was canceled.");
    if (subscriptions[_subscriptionId].chargeableAfterTime > block.timestamp){
      return uint256(subscriptions[_subscriptionId].chargeableAfterTime) - block.timestamp;
    }
    else {
      return 0;
    }
  }

  /// @dev internal function that actually does the merchant address update
  function setMerchantAddress(address _merchant) internal {
    pendingMerchantAddressUpdate = address(0);
    merchant = _merchant;
    emit MerchantAddressUpdate(_merchant);
  }

  /// @dev This enforces a lower limit of 1 day on the cooldown. This is essential due to the variance in block
  ///   timestamps; a minimum of 1 day makes the possible variance negligible
  function getCooldown(uint256 _cooldownIdx) internal view returns(uint256) {
    uint256 cooldown = cooldowns[_cooldownIdx];
    if (cooldown < 1 days) {
      cooldown = 1 days;
    }
    return cooldown;
  }

  function getOwnerCut(uint256 _cost, uint256 _bp) internal pure returns(uint256) {
    // Any subscription with a cost such that (_cost * _bp) < 10000 will return 0 for the factory cut.
    // This is okay since most erc20 tokens use 10^18 decimals so such a subscription cost would be minuscule and not
    // worth the gas to charge in the first place.
    return (_cost  * _bp) / 10000;
  }

}

