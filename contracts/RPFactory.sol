// SPDX-License-Identifier: MIT
pragma solidity >=0.8.12;

import "@openzeppelin/contracts/access/Ownable.sol";
import "./RP.sol";

/// @title A factory for generating and managing RP (Recurring Payment) contracts
/// @author Zan Aslett - RedSky Technologies
/// @notice This contract is to be used for managing merchants' RP contracts
/// @dev All function calls are external and callable only by contract owner
contract RPFactory is Ownable {
  event RPContractDeployed(uint256 indexed id, address indexed merchant, address contractAddress, uint256 numTokens, uint256 numCooldowns);
  event RPContractLockChanged(uint256 indexed id, bool isLocked);

  address[] public RPs;

  /// @notice To be used by registered merchants of the Factory contract owner
  /// @notice It is strongly recommended to use only stablecoins as payment tokens
  /// @notice The RP contract will limit the number of charges to once per day, meaning the min value in _cooldowns should be >= 1 day
  /// @param _tokens list of ERC20 compliant token contract addresses
  /// @param _cooldowns list of allowed cooldowns or charge intervals in seconds. The minimum value must be greater than 1 day
  function deployRP(address[] calldata _tokens, uint256[] calldata _cooldowns, uint256 _ownerFeeBp) external {
    RP rp = new RP(msg.sender, _tokens, _cooldowns, _ownerFeeBp);
    RPs.push(address(rp));
    emit RPContractDeployed(RPs.length - 1, msg.sender, address(rp), _tokens.length, _cooldowns.length);
  }

  /// @notice locks/unlocks a specific RP contract in case of security breach
  /// @dev Query past events to retrieve _rpId
  /// @param _rpId The id of the rp contract instance
  /// @param _isToBeLocked Whether the function call will lock or unlock a contract
  function setRPLock(uint256 _rpId, bool _isToBeLocked) external onlyOwner {
    RP(RPs[_rpId]).setIsLocked(_isToBeLocked);
    emit RPContractLockChanged(_rpId, _isToBeLocked);
  }

  /// @notice to be used in customer support situations
  /// @param _rpId The id of the rp contract instance
  /// @param _subscriptionId the subscriptionId to be canceled within the context of the rp contract instance
  function proxyCancelSubscription(uint256 _rpId, uint256 _subscriptionId) external onlyOwner {
    RP(RPs[_rpId]).cancelSubscription(_subscriptionId);
  }

  /// @notice To be used for automatically charging subscriptions in a centralized way
  /// @param _rpId The id of the rp contract instance
  /// @param _subscriptionId the subscriptionId to be canceled within the context of the rp contract instance
  function proxyChargeSubscription(uint256 _rpId, uint256 _subscriptionId) external onlyOwner {
    RP(RPs[_rpId]).chargeSubscription(_subscriptionId);
  }

  /// @notice to withdraw any erc20 tokens sent to this contract
  /// @param _erc20Token Address of token to be transferred to the contract owner
  function withdrawFunds(address _erc20Token) external onlyOwner {
    IERC20 tokenInterface = IERC20(_erc20Token);
    uint256 balance = tokenInterface.balanceOf(address(this));
    tokenInterface.transfer(owner(), balance);
  }

  /// @notice to set the factory cut of a specific RP instance
  /// @dev enforces a maximum cut of 5% to protect against redirecting entire payment to factory owner
  /// @param _rpId The id of the rp contract instance
  /// @param _bp The number of basis points for the fee 0.01% = 1bp => 1.50% = 150bp
  function setFactoryFee(uint256 _rpId, uint256 _bp) external onlyOwner {
    require(_bp <= 500, "Cannont set a fee higher than 5%");
    RP(RPs[_rpId]).setOwnerFee(_bp);
  }

  /// @notice Updates the RP contract's owner address
  /// @notice Only to be used in customer support situations
  /// @param _rpId The id of the rp contract instance
  /// @param _merchant The new merchant address
  function dangerouslyForceSetMerchantAddress(uint256 _rpId, address _merchant) external onlyOwner {
    RP(RPs[_rpId]).forceMerchantAddressUpdate(_merchant);
  }

}
