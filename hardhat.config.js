require("@nomiclabs/hardhat-waffle");
require("hardhat-gas-reporter");
require('hardhat-abi-exporter');
const secrets = require('./secrets.json');

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
    defaultNetwork: "hardhat",
    networks: {
      hardhat: {},
      mumbai: {
          url: "https://rpc-mumbai.maticvigil.com",
          accounts: [secrets.mumbaiTestnetPK]
      }
    },
    solidity: "0.8.12",
    paths: {
      sources: "./contracts",
      tests: "./test",
      cache: "./cache",
      artifacts: "./artifacts"
    },
    mocha: {
      timeout: 3000000
    },
    gasReporter: {
        token: 'MATIC',/*'ETH',*/
        currency: 'USD',
        coinmarketcap: secrets.coinMarketCapApiKey
    },
    abiExporter: {
      path: './abi',
      runOnCompile: true,
      clear: true,
      only: ["RP", "RPFactory", "SimpleErc20"],
      flat: true,
      spacing: 2,
      pretty: true
    }
};